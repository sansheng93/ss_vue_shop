import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 导入全局样式表
import './assets/css/global.css'
import axios from 'axios'
import TreeTable from 'vue-table-with-tree-grid'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器对应的样式表
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

// **TODO** 使用UmyUI的table组件
// import UmyUi from 'umy-ui'
// import 'umy-ui/lib/theme-chalk/index.css'
// Vue.use(UmyUi)

// 配置请求的基准路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
// 设置请求拦截器, 在请求头中绑定 token
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // console.log(config)
  // 语法: 必须return config
  return config
})
Vue.prototype.$http = axios

Vue.config.productionTip = false

Vue.component('tree-table', TreeTable)

// 将富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)

// 注册全局过滤器
Vue.filter(
  // 日期格式化
  'dateFormat', function (originVal) {
    const date = new Date(originVal)
    const y = date.getFullYear()
    // padStart()函数控制字符串格式, 参数代表含义: 为2位, 不足则在首位补'0'
    const m = (date.getMonth() + 1 + '').padStart(2, '0')
    const d = (date.getDate() + '').padStart(2, '0')
    const hh = (date.getHours() + '').padStart(2, '0')
    const mm = (date.getMinutes() + '').padStart(2, '0')
    const ss = (date.getSeconds() + '').padStart(2, '0')
    // return `yyyy-mm-dd hh:mm:ss`
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
  })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
